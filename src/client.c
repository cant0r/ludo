#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

#define BUFSIZE 1024
#define PORT_NO 2001
#define error(a,b) fprintf(stderr, a, b)

#define RED   "\x1B[31m"
#define GRN   "\x1B[32m"
#define YEL   "\x1B[33m"
#define MAG   "\x1B[35m"
#define CYN   "\x1B[36m"
#define RESET "\x1B[0m"

typedef enum RESPONSE
{
    GAMEOVER = -1,
    EMPTY = 0,
    GAMEDATA = 1,
    ACTION = 2,
    UPDATE = 3
} Response;

typedef struct figure
{
    int pos;
    char color[6];
} Figure;

void handleInterrrupt(int sig);

void createClient(char* name, int* fd);
void notifyServer(int* server, char* msg, int len, int flags);
void waitForResponse(int* server, void* buffer, int flags);
void parseGameData(char* data, Figure* figures);
Response processResponse(char* response);
void setUp(int argc, char* addr, char* name);
void usage(int err);

int gameOver(char* response);

void drawBorder(char c);
char* getColor(char* color);
void printFigures(Figure figs[4]);
void endGame(char* result);


char* name;

int main(int argc, char *argv[] ) {// arg count, arg vector   

    
   /* Declarations */
   
   int serverFD;	                       // socket endpt	
   int flags;                      // rcv flags
   struct sockaddr_in server;	     // socket name (addr) of server 
   int server_size;                // length of the socket addr. server 
   int err;                        // error code
   int ip;						   // ip address
   char on;                        // 
   char buffer[BUFSIZE+1];         // datagram dat buffer area
   char server_addr[16];           // server address	
   Figure figures[4];

   
   setUp(argc,argv[1],argv[2]);
   
   /* Initialization */
   name = argv[2];
   on    = 1;
   flags = 0;
   server_size = sizeof server;
   sprintf(server_addr, "%s", argv[1]);
   ip = inet_addr(server_addr);
   server.sin_family      = AF_INET;
   server.sin_addr.s_addr = ip;
   server.sin_port        = htons(PORT_NO);
   signal(SIGINT,handleInterrrupt);
   
   /* User interface */
  
   /* Creating socket */
   createClient(argv[2], &serverFD);
   /* Setting socket options */
   setsockopt(serverFD, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on);
   setsockopt(serverFD, SOL_SOCKET, SO_KEEPALIVE, (char *)&on, sizeof on);
  

   /* Connecting to the server */
   err = connect(serverFD, (struct sockaddr *) &server, server_size);
   if (err < 0) {
      error("%s: Cannot connect to the server.\n", argv[0]);
      exit(2);
   }
   
   //Sending it our name
   notifyServer(&serverFD, name, strlen(name)+1, flags);
   waitForResponse(&serverFD, buffer, flags);
  
   while(strstr(buffer, "Szia") == 0)
   {
       printf("%s\n", buffer);
       scanf("%s", buffer);
       sprintf(name,"%s",buffer);
       notifyServer(&serverFD, buffer, strlen(buffer)+1, flags);
       waitForResponse(&serverFD, buffer, flags);
       
   }
  
   printf("%s\n", buffer);
   while(1)
   {
        waitForResponse(&serverFD, buffer, flags);
        int response = processResponse(buffer);
        if(response == GAMEOVER)
        {
            drawBorder('$');
            endGame(buffer);
            drawBorder('$');
            printf("Végeredmény:\n");
            waitForResponse(&serverFD, buffer, flags);
            parseGameData(buffer,figures);
            printf("\tTIÉD #\t\tTIÉD #\t\tELLENSÉGÉ #\tELLENSÉGÉ #\n");
            printFigures(figures);
            drawBorder('-');
            break;
        }
        else if(response == GAMEDATA)
        {
            parseGameData(buffer, figures);
            continue;
        }
        if(response == UPDATE)
        {
            if(strstr(buffer, "Vége"))
            {
                printf("%s%s\n", getColor("RESET"),buffer);
                drawBorder('#');
            }
            else if(strstr(buffer, "WOW"))
            {
                 printf("%s%s", getColor("Kék"),buffer);
            }
            else if(strstr(buffer, "Határt!"))
            {
                 printf("%s%s", getColor("Sárga"),buffer);
            }
            else
            {
                 printf("%s%s", getColor("Piros"), buffer);
            }
            printf("\n%s", getColor("RESET"));
            continue;
        }
        drawBorder('-');
        printf("Jelenlegi állás\n");
        printf("\tTIÉD #\t\tTIÉD #\t\tELLENSÉGÉ #\tELLENSÉGÉ #\n");
        printFigures(figures);
        printf("%s : ", buffer);
        scanf("%s", buffer);
        if(processResponse(buffer) == ACTION)
        {
            notifyServer(&serverFD, buffer,strlen(buffer)+1, flags);
            scanf(" %s", buffer);
            notifyServer(&serverFD, buffer, strlen(buffer)+1,flags);
            waitForResponse(&serverFD, buffer, flags);
            //waiting for how much we rolled for our figure
            //win condition can happen here
            if(processResponse(buffer) == GAMEOVER)
            {
                drawBorder('$');
                endGame(buffer);
                drawBorder('$');
                printf("Végeredmény:\n");
                waitForResponse(&serverFD, buffer, flags);
                parseGameData(buffer,figures);
                printf("\tTIÉD #\t\tTIÉD #\t\tELLENSÉGÉ #\tELLENSÉGÉ #\n");
                printFigures(figures);
                drawBorder('-');
                break;
            }
            printf("%s%s%s\n", getColor("Zöld"),buffer,getColor("RESET"));
        }
        else
        {
            notifyServer(&serverFD, buffer, strlen(buffer)+1, flags);
        }
        
   }
 
   /* Closing sockets and quit */
   close(serverFD);
   exit(0);
}
void handleInterrrupt(int sig)
{
    //overriding default handler
    //basically doing nothing
}
void createClient(char* name, int* fd)
{
   *fd = socket(AF_INET, SOCK_STREAM, 0);
   if (fd < 0) 
   {
      error("%s: Socket creation error.\n",name);
      exit(1);
   }

}
void notifyServer(int* server, char* msg, int len, int flags)
{
   if(len>BUFSIZE+1)
   {
        sprintf(msg, "túlcsordulás"); //hehe
        len = strlen(msg)+1;
   }
    
   char buf[255];
   sprintf(buf,"%d",len);
   send(*server,buf,sizeof(buf),flags);
   
   int writtenBytes = send(*server,msg,len, flags);
   if (writtenBytes < 0)
        {
            error("server: Cannot send data to the %s.\n","client");
            exit(6);
        }
}
void waitForResponse(int* server, void* msg, int flags)
{
    char buf[255];
    if(recv(*server,buf,sizeof(buf),flags) <= 0)
    {
        error("%s: game session has been terminated by an unknown being\n", name);
        exit(-1);
    }
    int all = atoi(buf);
    
    while (all != 0)
    {

        int num = recv(*server, msg, all, flags);
    
        if (num < 0)
        {
            error("%s something bad happened!\n",name);
            exit(1);
        }
        else if (num > 0)
        {
            msg += num;
            all -= num;
        }
  }
    
}
Response processResponse(char* response)
{
   if(gameOver(response))
   {
        return GAMEOVER;
   }
   if(strstr(response, "#"))
   {
        return GAMEDATA;
   }
   if(strstr(response, "lépj") && strstr(response, "jössz") == 0)
   {
       return ACTION;
   }
   if(strstr(response, "üt") || strstr(response,"Határt!") ||
      strstr(response,"bábúddal") || strstr(response, name) == NULL ||
      strstr(response, "Vége"))
   {
        return UPDATE;
   }
   return EMPTY;
}
void setUp(int argc, char* addr, char* name)
{
    if(argc != 3)
        usage(0);
    else if(strcmp(addr,"127.0.0.1") != 0)
        usage(1);
    else if(strlen(name) < 3)
        usage(2);
    else if(strlen(name)>16)
        usage(3);
    else
        return;
}
void usage(int errno)
{
    switch(errno)
    {
        case 0: printf("Túl kevés argumentumot adtál meg!\n"); break;
        case 1: printf("Érvénytelen IP cím! Jelenleg csak 127.0.0.1 támogatott!\n"); break;
        case 2: printf("Túl rövid a neved!\n"); break;
        case 3: printf("Túl hosszú a neved!\n"); break;
        default: break;
    }
    
    printf("Usage: ./client server_ip nameOfClient\n");
    exit(-2);
}
int gameOver(char* response)
{
    return strstr(response, "nyertél") || strstr(response,"feladtad")
     || strstr(response, "megnyerted") || strstr(response, "Vesztettél");
}
void parseGameData(char* data, Figure* figures)
{

    sscanf(data,"# %s %d %s %d %s %d %s %d", 
           figures[0].color, &(figures[0].pos),
           figures[1].color, &(figures[1].pos),
           figures[2].color, &(figures[2].pos),
           figures[3].color, &(figures[3].pos)
    );
    
}
void drawBorder(char c)
{
    for(int i=0; i<80; i++)
        putchar(c);
    putchar('\n');
}
char* getColor(char* color)
{
    if(!strcmp(color, "Piros"))
        return RED;
    if(!strcmp(color,"Zöld"))
        return GRN;
    if(!strcmp(color, "Kék"))
        return CYN;
    if(!strcmp(color, "Sárga"))
        return YEL;
    if(!strcmp(color, "val"))
        return MAG;
    else
        return RESET;
}
void printFigures(Figure figs[4])
{
    for(int i=0; i<4; i++)
    {
        printf("\t%s%s %s%d\t", getColor(figs[i].color), figs[i].color, getColor("val"), figs[i].pos);
    }
    printf("%s\n", RESET);
}
void endGame(char* result)
{
    if(strstr(result, "nyertél") != 0 || strstr(result, "megnyerted") != 0)
    {
        printf("\t\t\t%s%s!", getColor("Zöld"), result);
    }
    else
    {
         printf("\t\t%s%s!", getColor("Piros"), result);
    }
    printf("%s\n",getColor("end"));
}
