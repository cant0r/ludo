#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h> 
#include <time.h>
#include <locale.h>
#include <signal.h>
#include <unistd.h>

#define BUFSIZE 1024                     
#define PORT_NO 2001     
#define LENGTH_OF_TRACK 50
#define SERVERNAME "Father Picard"
#define error(a,b) fprintf(stderr, a, b)

typedef enum RESPONSE
{
   GAMEOVER = -1,
   ACTION = 1,
   EMPTY = 0
    
} Response;

typedef struct figure
{
    int pos;
    char* color;
} Figure;

typedef struct player
{
    char* name;
    int hasToken;
    Figure figures[2];
    unsigned int numberOfFigures;
    
}Player;

void handleInterrupt(int sig);

void createServer(int* serverFD);
int connectClient(char* name, char* illegalName,int* clientFD, struct sockaddr_in* addr, socklen_t* size, int flags);
void notifyClient(int* client, char* msg, int len, int flags);
void waitForResponse(int* client, void* buffer, int flags);
int processResponse(char* response);
int log(char* msg, char* sender);
void sendGameData(char* buffer, Player* p1, Player* p2);

Player createPlayer(char* name, char* color_one, char* color_two, int token);
int roll();
int move(Figure* f);
void handleCollision(Player* one, Player* two, int* attacker, int* defender);
int handleBorderControl(Figure* one, Figure* lastInArray, int* fd);
Figure* getFigureByColor(Player* figureOwner, char* color);
void gameOver(Player* winner, Player* loser,int* winnnerFD, int* loserFD, int flags);
void giveUp(int* loserFd, int* winnerFD, Player* loser, Player* winner, char* buffer, int flags);
int takeMoveAction(int* p1FD, int* p2FD, Player* p1, Player* p2, char* buffer, int flags);

int main(int argc, char *argv[] ){ 	

   signal(SIGINT, handleInterrupt);
   srand(time(NULL));  
    
   /* Declarations */
   int serverFileDescriptor;  // socket endpt, file descriptor
   int clientAFlieDescriptor;
   int clientBFlieDescriptor;
   int flags;                 // rcv flags
   struct sockaddr_in server; // socket name (addr) of server
   
   struct sockaddr_in player1Client; // Player 1
   struct sockaddr_in player2Client; //Player 2
   int server_size;                
   int client_size;   
   int response;
   int err;                        
   char on;                        
   char buffer[BUFSIZE+1];
   char client1Name[BUFSIZE+1];
   char client2Name[BUFSIZE+1];
   Player player1, player2;

   /* Initialization */
   on                     = 1;
   flags                  = 0;
   server_size            = sizeof server;
   client_size            = sizeof player1Client;
   server.sin_family      = AF_INET;
   server.sin_addr.s_addr = INADDR_ANY;
   server.sin_port        = htons(PORT_NO);
    
   /* Creating socket */
   log("Szerver indítása", SERVERNAME);
   createServer(&serverFileDescriptor);
  
   setsockopt(serverFileDescriptor, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
   setsockopt(serverFileDescriptor, SOL_SOCKET, SO_KEEPALIVE, &on, sizeof on);
 
   
   /* Binding socket */
   err = bind(serverFileDescriptor, (struct sockaddr *) &server, server_size);
   if (err < 0) 
   {
      error("%s: Cannot bind to the Server socket\n", SERVERNAME);
      exit(2);
   }
      
   /* Listening */
   log("Letapogatás megkezdése", SERVERNAME);
   err = listen(serverFileDescriptor, 10);
   if (err < 0) 
   {
      error("%s: Cannot listen to the socket",argv[0]);
      exit(3);
   }

   //Accept Player 1
   clientAFlieDescriptor = connectClient(client1Name,SERVERNAME,&serverFileDescriptor,&player1Client, &client_size, flags);
   player1 = createPlayer(client1Name, "Piros", "Zöld", 1);
   
   
   notifyClient(&clientAFlieDescriptor,"Szia! A másik kliens csatlakozásával indul a játék!", strlen("Szia! A másik kliens csatlakozásával indul a játék!")+1,flags);
   log("1. játékos csatlakozott!", SERVERNAME);
   
    
   //Accept Player 2
   clientBFlieDescriptor = connectClient(client2Name,player1.name,&serverFileDescriptor,&player2Client, &client_size, flags);
   player2 = createPlayer(client2Name, "Kék", "Sárga", 0);
   
   sprintf(buffer,"Szia! Indul a játék! %s kezd!",player1.name);
   notifyClient(&clientBFlieDescriptor,buffer, strlen(buffer)+1,flags);
   log("2. játékos is csatlakozott!", SERVERNAME);
   
   while(1)
   {
       
       if(player1.hasToken)
       {  
            do
            {
                sendGameData(buffer,&player1, &player2);
                notifyClient(&clientAFlieDescriptor, buffer, strlen(buffer)+1, flags);
                sprintf(buffer, "%s, te jössz! Opcióid: feladom, lépj <BÁBÚ>", player1.name);
                notifyClient(&clientAFlieDescriptor, buffer,strlen(buffer)+1, flags);
                waitForResponse(&clientAFlieDescriptor, buffer, flags);
                log(buffer, player1.name);
                if(!clientBFlieDescriptor)
                    break;
            } while(!(response = processResponse(buffer)));
            
            if(response == GAMEOVER)
            {
                giveUp(&clientAFlieDescriptor, &clientBFlieDescriptor, &player1, &player2, buffer, flags);
                break;
            }
            else if(response == ACTION)
            {
              if(!takeMoveAction(&clientAFlieDescriptor, &clientBFlieDescriptor, &player1, &player2, buffer, flags))
               {
                     gameOver(&player1, &player2,&clientAFlieDescriptor, &clientBFlieDescriptor, flags);
                     break;
               }
            }
            
            sprintf(buffer, "Vége a körödnek! Most %s köre jön!", player2.name);
            notifyClient(&clientAFlieDescriptor, buffer,strlen(buffer)+1, flags);
            player1.hasToken = 0;
            player2.hasToken = 1;
       }
       else
       {
           do
            {
                sendGameData(buffer,&player2, &player1);
                notifyClient(&clientBFlieDescriptor, buffer,strlen(buffer)+1, flags);
                sprintf(buffer, "%s te jössz! Opcióid: feladom, lépj <BÁBÚ>", client2Name);
                notifyClient(&clientBFlieDescriptor, buffer, strlen(buffer)+1,flags);
                waitForResponse(&clientBFlieDescriptor, buffer, flags);
                log(buffer,player2.name);
                if(!clientAFlieDescriptor)
                    break;
                
            } while(!(response = processResponse(buffer)));
            
            if(response == GAMEOVER)
            {
                giveUp(&clientBFlieDescriptor, &clientAFlieDescriptor, &player2, &player1, buffer, flags); 
                break;
            }
            else if(response == ACTION)
            {
               if(!takeMoveAction(&clientBFlieDescriptor, &clientAFlieDescriptor, &player2, &player1, buffer, flags))
               {
                     gameOver(&player2, &player1,&clientBFlieDescriptor, &clientAFlieDescriptor, flags);
                     break;
               }
            }
           sprintf(buffer, "Vége a körödnek! Most %s köre jön!", player1.name);
           notifyClient(&clientBFlieDescriptor, buffer,strlen(buffer)+1, flags);
           player2.hasToken = 0;
           player1.hasToken = 1;
           
       }
       
   }
   

   log("!!!Játék vége!!!", SERVERNAME);
   /* Closing sockets and quit */
   usleep(20);
   close(clientAFlieDescriptor);
   log("Kliens 1 leállítva!", SERVERNAME);
   close(clientBFlieDescriptor);
   log("Kliens 2 leállítva!", SERVERNAME);
   close(serverFileDescriptor);
   log("Szerver leállítva!", SERVERNAME);
   return 0;
}
void handleInterrupt(int sig)
{
        //do absolutely nothing
}
void createServer(int* serverFD)
{
    *serverFD = socket(AF_INET, SOCK_STREAM, 0 );
    if (*serverFD < 0) 
    {
      error("%s: Server Socket creation error\n", SERVERNAME);
      exit(1);
    }
    

}
int connectClient(char* name, char* illegalName, int* serverFD, struct sockaddr_in* addr, socklen_t* size, int flags)
{
    char buffer[255];
    int socket= accept(*serverFD, (struct sockaddr *) addr, size);
    
    if (socket < 0) 
    {
      error("%s: Cannot accept on socket\n",SERVERNAME);
      exit(4);
    }
    waitForResponse(&socket,name,flags);
    while(strstr(name,illegalName) != NULL || strstr(name,SERVERNAME) != NULL)
    {
      sprintf(buffer, "A %s név már foglalt vagy része a nevednek! Adj meg egy másik nevet: ", (strstr(name,illegalName) != NULL) ? illegalName : SERVERNAME);
      notifyClient(&socket, buffer, strlen(buffer)+1, flags);
      waitForResponse(&socket, name, flags);
    }
    while(strlen(name)<3 || strlen(name)>16)
    {
      sprintf(buffer, "A nevednek legalább 3 karakteresnek kell lennie, de legfeljebb 16 karakterből állhat!\nAdj meg egy másikat!");
      notifyClient(&socket, buffer, strlen(buffer)+1, flags);
      waitForResponse(&socket, name, flags);  
    }
    return socket;
}
Player createPlayer(char* name,  char* color_one,  char* color_two, int token)
{
    Player thePlayer;
    thePlayer.name = name;
    thePlayer.hasToken = (token) ? 1 : 0;
    thePlayer.figures[0].pos = 0;
    thePlayer.figures[0].color = color_one;
    thePlayer.figures[1].pos = 0;
    thePlayer.figures[1].color = color_two;
    thePlayer.numberOfFigures = 2;
    return thePlayer;
}
int roll()
{
    return 1 + rand() % 6;
}
int move(Figure* f)
{
    int r = roll();
    f->pos += r;
    return r;
    
}
void handleCollision(Player* one, Player* two, int* attacker, int* defender)
{
    char buffer[BUFSIZE+1];
    
    for(int i=0; i<one->numberOfFigures; i++)
    {
        for(int j=0; j<two->numberOfFigures; j++)
        {
            if(one->figures[i].pos == two->figures[j].pos)
            {
            
                if(!one->figures[i].pos)
                    continue;
            
                sprintf(buffer, "A collision has happened between %s and %s at position %d", one->figures[i].color,
                                two->figures[j].color,   
                                one->figures[i].pos);
                log(buffer, SERVERNAME);
                sprintf(buffer, "Leüttötted %s %s bábúját! WOW!", two->name,
                        two->figures[j].color);
                notifyClient(attacker, buffer, strlen(buffer)+1, 0);
                sprintf(buffer, "Leüttötte %s a %s bábújával a %s bábúdat! HOP", one->name, one->figures[i].color, two->figures[j].color);
                notifyClient(defender, buffer, strlen(buffer)+1, 0);
                two->figures[j].pos = 0; 
            }
        }
        
        
    }
    
}
int handleBorderControl(Figure* one, Figure* lastInArray, int* fd)
{
    char buffer[255];
    if(one->pos >= LENGTH_OF_TRACK)
            {
                
                sprintf(buffer, "A %s bábúddal átlépted a Határt!", one->color);
                notifyClient(fd, buffer, strlen(buffer)+1, 0);
                
                
                if(strcmp(one->color, lastInArray->color) !=0 )
                {
                    Figure temp = *lastInArray;
                    *lastInArray = *one;
                    *one = temp;
                }
                return 1;
            }
            return 0;
}
void notifyClient(int* client, char* msg, int len, int flags)
{
   char buf[255];
   sprintf(buf,"%d",len);
   if(send(*client,buf,sizeof(buf),flags) < 0)
   {
        error("%s: Game session has been terminated by an unknown being!\n",SERVERNAME);
       exit(-1);
   }
   int writtenBytes = send(*client,msg,len, flags);
   
   if (writtenBytes <= 0)
        {
            error("%s: Cannot send data to the client.\n",SERVERNAME);
            exit(6);
        }
}
void waitForResponse(int* client, void * msg, int flags)
{
    char buf[255];
    if(recv(*client,buf,sizeof(buf),flags) <= 0)
    {
        log("Megszakadt a kapcsolat a klienssel! Játék leállítása..",SERVERNAME);
        exit(-1);
    }
    int all = atoi(buf);
    
    while (all != 0)
    {

        int num = recv(*client, msg, all, flags);

        if (num < 0)
        {
            error("%s something bad happened during recv()!\n",SERVERNAME);
            exit(1);
        }
        else if (num > 0)
        {
            msg += num;
            all -= num;
        }
  }
    
}
int log(char* msg, char* sender)
{
     time_t now = time(NULL);
     struct tm * eventTime = localtime(&now);
    
     return printf("[%04d.%02d.%02d. %02d:%02d:%02d] %s üzente:\n",
                   eventTime->tm_year + 1900,
                   eventTime->tm_mon+1,
                   eventTime->tm_mday,
                   eventTime->tm_hour,
                   eventTime->tm_min,
                   eventTime->tm_sec, sender
                  ) + printf("%s\n", msg);
}
void sendGameData(char* buffer, Player* p1, Player* p2)
{
    sprintf(buffer, "# %s %d %s %d %s %d %s %d",
        p1->figures[0].color,  p1->figures[0].pos,
        p1->figures[1].color,  p1->figures[1].pos,
        p2->figures[0].color,  p2->figures[0].pos,
        p2->figures[1].color,  p2->figures[1].pos
    );
}
int processResponse(char* response)
{
    
   if(strstr(response, "feladom") != 0)
   {
        return GAMEOVER;
   }
   if(strstr(response, "lépj" ) != 0)
   {
        return ACTION;
   }
   return EMPTY;
    
    
}
Figure* getFigureByColor(Player* figureOwner, char* color)
{
    for(int i=0; i<figureOwner->numberOfFigures; i++)
    {
        if(strcmp(figureOwner->figures[i].color, color) == 0)
            return &(figureOwner->figures[i]);
    }
    return 0;
}
void gameOver(Player* winner, Player* loser, int* winnerFD, int* loserFD, int flags)
{
    char buffer[BUFSIZE+1];
    sprintf(buffer, "!!%s, megnyerted a játékot!!",winner->name);
    notifyClient(winnerFD, buffer, strlen(buffer)+1,flags);
    sendGameData(buffer, winner, loser);
    notifyClient(winnerFD,buffer, strlen(buffer)+1,flags);
    sprintf(buffer, "%s nyerte a játszmát! Vesztettél %s!", winner->name, loser->name);
    notifyClient(loserFD, buffer, strlen(buffer)+1, flags);
    sendGameData(buffer, loser, winner);
    notifyClient(loserFD,buffer, strlen(buffer)+1,flags);
}
void giveUp(int* loserFd, int* winnerFD, Player* loser, Player* winner, char* buffer, int flags)
{
    sprintf(buffer, "%s, feladtad a játékot! %s győzedelmes!", loser->name, winner->name);
    notifyClient(loserFd, buffer,  strlen(buffer)+1,flags);
    sendGameData(buffer, loser, winner);
    notifyClient(loserFd,buffer, strlen(buffer)+1,flags);
    sprintf(buffer, "%s feladta! Te nyertél %s!!!", loser->name, winner->name);
    notifyClient(winnerFD, buffer,  strlen(buffer)+1,flags);
    sendGameData(buffer, winner, loser);
    notifyClient(winnerFD,buffer, strlen(buffer)+1,flags);
}
int takeMoveAction(int* p1FD, int* p2FD, Player* p1, Player* p2, char* buffer, int flags)
{
    waitForResponse(p1FD, buffer, flags); 
    printf("%s\n", buffer); 
    Figure* chosenOne = getFigureByColor(p1,buffer); 
    if(chosenOne == NULL)
       chosenOne = &(p1->figures[0]);
    int progress = move(chosenOne);
    sprintf(buffer, "A %s színű bábúddal %d-t léptél előre!",  chosenOne->color, progress);
    notifyClient(p1FD, buffer, strlen(buffer)+1, flags);
    sprintf(buffer, "%s a %s bábújával %d-t lépett előre!", p1->name,
            chosenOne->color, progress);
    notifyClient(p2FD, buffer, strlen(buffer)+1, flags);
    handleCollision(p1,p2, p1FD, p2FD);
    if(handleBorderControl(chosenOne, &p1->figures[1], p1FD))
        p1->numberOfFigures-=1;
    if(!p1->numberOfFigures)
        return 0;
    
    
    return 1;
}








